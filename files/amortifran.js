const precio = document.getElementById("precio");
const inicial = document.getElementById("inicial");
const tea = document.getElementById("tea");
const meses = document.getElementById("meses");

const calcular = document.getElementById('calcular');
const reset = document.getElementById('reset');

const pagoMensual = document.getElementById('pagoMensual')
const totalInteres = document.getElementById('totalInteres')
const totalPagos = document.getElementById('totalPagos')

const resultados = document.getElementById('resultados');

const alertas = document.getElementById('alertas')

calcular.addEventListener('click', () => {
  calcularpago(precio.value, inicial.value, tea.value, meses.value);
})

reset.addEventListener('click', () => {
  resultados.innerHTML = "",
    precio.value = '',
    inicial.value = '',
    tea.value = '',
    meses.value = '',
    pagoMensual.value = '',
    totalInteres.value = '',
    totalPagos.value = ''
})

function calcularpago(precio, inicial, tea, meses) {

  while (resultados.firstChild) {
    resultados.removeChild(resultados.firstChild);
  }

  let saldo = precio - inicial

  let interes = 0,
    amort = 0,
    pago = 0,
    desgra = 0,
    pagoTotal = 0

  pago = saldo * (
    Math.pow(1 + tea / 100, meses) * tea / 100) / (Math.pow(1 + tea / 100, meses) - 1
    );

  pagoMensual.value = pago.toFixed(2)

  const rowOne = document.createElement('tr');
  rowOne.innerHTML = `
        <td>0</td>
        <td></td>
        <td></td>
        <td></td>
        <td>${saldo.toFixed(2)}</td>
        <td></td>
        <td></td>
    `;

  if (
    precio === '',
    inicial === '',
    tea === '',
    meses === ''
  ) {
    alertas.innerHTML = "Completar todos los campos"
    alertas.style.display = "block"
  }
  else if (inicial <= precio * 0.80 && inicial >= precio * 0.20) {
    alertas.style.display = "none"
    resultados.appendChild(rowOne)
    for (let i = 1; i <= meses; i++) {

      interes = parseFloat(saldo * (tea / 100));
      amort = pago - interes;
      saldo = parseFloat(saldo - amort);

      desgra = saldo * 0.005
      pagoTotal = desgra + pago

      const row = document.createElement('tr');
      row.innerHTML = `
          <td>${[i]}</td>
          <td>${amort.toFixed(2)}</td>
          <td class="interes">${interes.toFixed(2)}</td>
          <td>${pago.toFixed(2)}</td>
          <td>${saldo.toFixed(2)}</td>
          <td>${desgra.toFixed(2)}</td>
          <td>${pagoTotal.toFixed(2)}</td>
      `;
      resultados.appendChild(row)

      const filas = document.querySelectorAll(".interes");
      if (filas) {
        let total = 0;
        let totalFinal = 0
        filas.forEach((fila) => {
          total += parseFloat(fila.innerHTML);
        });
        totalFinal = parseFloat(precio) + total
        totalInteres.value = total.toFixed(2)
        totalPagos.value = totalFinal.toFixed(2)
      }

    }
  }
  else if (inicial <= precio * 0.20) {
    alertas.innerHTML = "El monto mínimo de la inicial debe ser 20% del valor del vehículo"
    alertas.style.display = "block"
  }
  else {
    alertas.innerHTML = "El monto máximo de la inicial debe ser 80% del valor del vehículo"
    alertas.style.display = "block"
  }

}

inicial.addEventListener('input', validar);

function validar() {
  if (inicial.value <= precio.value * 0.80 && inicial.value >= precio.value * 0.20) {
    alertas.innerHTML = ""
    alertas.style.display = "none"
    calcular.disabled = false
  }
  else if (inicial.value <= precio.value * 0.20) {
    alertas.innerHTML = "El monto mínimo de la inicial debe ser 20% del valor del vehículo"
    alertas.style.display = "block"
    calcular.disabled = true
  }
  else {
    alertas.innerHTML = "El monto máximo de la inicial debe ser 80% del valor del vehículo"
    alertas.style.display = "block"
    calcular.disabled = true
  }
}